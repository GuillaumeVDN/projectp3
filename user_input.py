# coding=utf-8

import time
from sense_hat import ACTION_PRESSED, DIRECTION_MIDDLE, DIRECTION_DOWN, DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_UP

import utils
import design

# -------------------- Demander un code  --------------------

def ask_code(sense):
    # ask for code
    design.display_color(sense, 0, (255, 127, 0)) # écran orange, en attente d'input
    code = ''
    last = 0.0
    while True:
        event = sense.stick.wait_for_event()
        if event.action == ACTION_PRESSED:
            # ignorer l'event si c'est trop récent (pour éviter les double clics)
            if time.time() - last < 0.6:
                continue
            last = time.time()
            # clicked on middle joystick, done ?
            if event.direction == DIRECTION_MIDDLE:
                design.display(sense, 0.5, design.Succes)
                return code
            # clic dans une autre direction, ajouter le mouvement au code
            elif event.direction == DIRECTION_UP:
                code += 'u'
                animation = design.FlecheHaut
            elif event.direction == DIRECTION_LEFT:
                code += 'l'
                animation = design.FlecheGauche
            elif event.direction == DIRECTION_RIGHT:
                code += 'r'
                animation = design.FlecheDroite
            elif event.direction == DIRECTION_DOWN:
                code += 'd'
                animation = design.FlecheBas
            else:
                continue
            # animation de confirmation
            design.display(sense, 0.5, animation)
            design.display_color(sense, 0, (255, 127, 0)) # écran orange, en attente d'input

# -------------------- Demander un texte  --------------------

characters = "abcdefghijklmnopqrstuvwxyz0123456789+-"  # + = confirm et - = reset

def ask_text(sense):
    text = ""
    index = 0
    __refresh_screen(sense, index)
    last = 0.0
    while True:
        event = sense.stick.wait_for_event()
        if event.action == ACTION_PRESSED:
            # up
            if event.direction == DIRECTION_UP:
                index += 5
                if index > 37:
                    index -= 38
                __refresh_screen(sense, index)
            # down
            elif event.direction == DIRECTION_DOWN:
                index -= 5
                if index < 0:
                    index += 38
                __refresh_screen(sense, index)
            # right
            elif event.direction == DIRECTION_RIGHT:
                index += 1
                if index > 37:
                    index -= 38
                __refresh_screen(sense, index)
            # left
            elif event.direction == DIRECTION_LEFT:
                index -= 1
                if index < 0:
                    index += 38
                __refresh_screen(sense, index)
            # middle
            elif event.direction == DIRECTION_MIDDLE:
                # ignorer l'event si c'est trop récent (pour éviter les double clics)
                if time.time() - last < 0.6:
                    continue
                last = time.time()
                # ajouter une lettre
                if index <= 35:
                    text += characters[index]
                    design.display_letter(sense, characters[index], 0.5, (255, 255, 0))
                    __refresh_screen(sense, index)
                # confirmer le message
                if index == 36:
                    return text
                # réinitialiser le message
                if index == 37:
                    text = ""
                    design.display_color(sense, 0.5, (255, 215, 0)) # écran jaune, confirmer le reset
                    __refresh_screen(sense, index)

def __refresh_screen(sense, index):
    sense.clear()
    if index <= 35:
        design.display_letter(sense, characters[index], 0, (255, 127, 0))
    elif index == 36:
        design.display(sense, 0, design.Succes)
    elif index == 37:
        design.display(sense, 0, design.Erreur)
