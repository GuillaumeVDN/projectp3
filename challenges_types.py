# coding=utf-8

from sense_hat import ACTION_PRESSED, DIRECTION_MIDDLE, DIRECTION_DOWN, DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_UP

import time
import random

import user_input
import utils
import design

__time_limit = 45.0

# -------------------- Type d'épreuve : réveiller (secouer le raspy) --------------------

def wake_up(sense):
    # pixelart
    design.display(sense, 0, design.MageDort)
    # l'utilisateur doit secouer le machin avant un temps imparti
    started = time.time()
    previous_time = started
    utils.log('\t\tEn attente de movement')
    while True:
        x, y, z = sense.get_accelerometer_raw().values()
        if x >= 3.0 or y >= 3.0 or z >= 3.0:
            utils.log('\t\tMouvement détecté, x = ' + str(x) + ', y = ' + str(y) + ', z = ' + str(z))
            # pixelart
            design.display(sense, 1, design.Mage1, design.Mage2)
            # succès
            return True
        # s'il a pris trop de temps, return false
        if time.time() - started >= __time_limit:
            utils.log('\t\tTrop de temps, plus de ' + str(__time_limit) + ', return')
            return False
        # log le mouvement toutes les secondes
        if previous_time != int(time.time()):
            previous_time = int(time.time())
            utils.log('\t\tx = ' + str(x) + ', y = ' + str(y) + ', z = ' + str(z))

# -------------------- Type d'épreuve : énigme (réponse à entrer) --------------------

__sphinx_enigmas = {
    'Réponse universelle': '42'
}

def sphinx(sense):
    # pixelart
    design.display(sense, 1.5, design.Sphynx)
    # l'utilisateur doit répondre à l'énigme du sphinx
    enigma = random.choice(__sphinx_enigmas.keys())
    answer = __sphinx_enigmas[enigma].lower()
    # succès
    utils.log('\t\tEn attente de réponse, enigma = ' + enigma)
    userinput = user_input.ask_text(sense).lower()
    if userinput == answer:
        utils.log('\t\tRéponse correcte')
        # pixelart
        design.display(sense, 0.5, design.Desert1, design.Desert2, design.Desert3)
        return True
    # fail
    utils.log('\t\tRéponse incorrecte')
    return False

# -------------------- Type d'épreuve : génie (souffler sur le raspy) --------------------

def genie(sense):
    # pixelart
    design.display(sense, 1.5, design.Lampe)
    # l'utilisateur doit réveiller le machin avant un temps imparti
    started = time.time()
    previous_time = started
    start_humidity = sense.get_humidity()
    target_humidity = start_humidity * 1.15
    utils.log('\t\tEn attente d\'humidité, start_humidity = ' + str(start_humidity) + ', target_humidity = ' + str(target_humidity))
    while True:
        humidity = sense.get_humidity()
        if humidity >= target_humidity:
            utils.log('\t\tHumidité détectée, humidity = ' + str(humidity))
            # pixelart
            design.display(sense, 1, design.Genie1, design.Genie2)
            return True
        # s'il a pris trop de temps, return false
        if time.time() - started >= __time_limit:
            utils.log('\t\tTrop de temps, plus de ' + str(__time_limit) + ', return')
            return False
        # log l'humidité toutes les secondes
        if previous_time != int(time.time()):
            previous_time = int(time.time())
            utils.log('\t\thumidity = ' + str(humidity))

# -------------------- Type d'épreuve : compagnon (choisir la bonne option) --------------------

options = [design.Argent, design.Femme, design.Chameau1]

def compagnon(sense):
    # log
    utils.log('\t\tEn attente de choix du compagnon')
    # afficher la première option disponible
    k = 1
    design.display(sense, 0, options[k - 1])
    # l'utilisateur doit sélectionner un choix du menu à l'aide du joystick
    while True:
        event = sense.stick.wait_for_event()
        if event.action == ACTION_PRESSED:
            # l'utilisateur a cliqué à droite
            if event.direction == DIRECTION_RIGHT:
                # augmenter l'index de l'option
                k += 1
                if k > 3:
                  k = 1
                # afficher la nouvelle option
                design.display(sense, 0, options[k - 1])
                # log
                utils.log('\tClic à droite, nouvelle option ' + str(k))
            # l'utilisateur a cliqué à gauche
            elif event.direction == DIRECTION_LEFT:
                # diminuer l'index de l'option
                k -= 1
                if k < 1:
                    k = 3
                # afficher la nouvelle option
                design.display(sense, 0, options[k - 1])
                # log
                utils.log('\tClic à gauche, nouvelle option ' + str(k))
            # l'utilisateur a cliqué au milieu : sélectionner l'option
            elif event.direction == DIRECTION_MIDDLE:
                utils.log('\tClic au milieu, choix de l\'option ' + str(k))
                # succès
                if k == 3:
                    # pixelart
                    design.display(sense, 1, design.Chameau1, design.Chameau2, design.DesertP1, design.DesertP2, design.DesertP3)
                    return True
                else:
                    return False

# -------------------- Type d'épreuve : porte (orienter le raspy) --------------------

def door(sense):
    # pixelart
    design.display(sense, 0, design.Pyramide)
    # l'utilisateur doit mettre le machin vers le haut avant un temps imparti
    started = time.time()
    previous_time = started
    utils.log('\t\tEn attente d\'orientation')
    while True:
        pitch, roll, yaw = utils.get_orientation(sense)
        pitch = utils.round_number(pitch, 90)
        roll = utils.round_number(roll, 90)
        yaw = utils.round_number(yaw, 90)
        if roll == 180:
            utils.log('\t\tOrientation détectée, pitch = ' + str(pitch) + ', roll = ' + str(roll) + ', yaw = ' + str(yaw))
            # pixelart
            design.display(sense, 1, design.Soleil)
            design.display(sense, 0.3, design.Porte1, design.Porte2, design.Porte3, design.Porte4, design.Porte5, design.Porte6, design.Porte7)
            # succès
            return True
        # s'il a pris trop de temps, return false
        if time.time() - started >= __time_limit:
            utils.log('\t\tTrop de temps, plus de ' + str(__time_limit) + ', return')
            return False
        # log l'orientation toutes les secondes
        if previous_time != int(time.time()):
            previous_time = int(time.time())
            utils.log('\t\tpitch = ' + str(pitch) + ', roll = ' + str(roll) + ', yaw = ' + str(yaw))
