# coding=utf-8

import os
import traceback
import datetime

# -------------------- Math --------------------

def round_number(number, multiple):
    return ((number + multiple / 2) / multiple) * multiple

# -------------------- Raspberry  --------------------

def get_orientation(sense):
    o = sense.get_orientation()
    pitch = o["pitch"]
    roll = o["roll"]
    yaw = o["yaw"]
    return (int(pitch), int(roll), int(yaw))

def shutdown():
    # appeler la commande shutdown en tant que superopérateur
    os.system("sudo shutdown -P now")

# -------------------- Log  --------------------

def log(line):
    # print to console
    print(line)
    # save to file
    try:
    	with open('__log.txt', 'a+') as file:
    		file.write('\n' + '[' + datetime.datetime.now().strftime('%Y-%m-%d à %H:%M:%S') + '] ' + line)
    except:
        print('\n\n\t\t\tImpossible de sauvegarder le log :')
        traceback.print_exc()
        print('\n\n')
