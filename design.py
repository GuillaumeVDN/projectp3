# coding=utf-8

import time

# -------------------- Afficher une animation --------------------

def display(sense, waiting_time, *animations):
    sense.clear()
    for animation in animations:
        sense.set_pixels(animation())
        if waiting_time > 0.0:
            time.sleep(waiting_time)

def display_color(sense, waiting_time, color):
    sense.clear()
    sense.set_pixels(get_pixels(color))
    if waiting_time > 0.0:
        time.sleep(waiting_time)

def display_letter(sense, letter, waiting_time, color):
    sense.clear()
    sense.show_letter(letter, text_colour = color)
    if waiting_time > 0.0:
        time.sleep(waiting_time)

# -------------------- Divers --------------------

def get_pixels(color):
    result = []
    for i in range(64):
        result.append(color)
    return result

# -------------------- Couleurs --------------------

green = (0, 255, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
white = (255, 255, 255)
nothing = (0, 0, 0)
pink = (255, 205, 150)
grey = (32, 32, 32)
greyf = (10, 10, 10)
brun = (102, 51, 0)
sable = (255, 178, 102)

# -------------------- Designs --------------------

def MageDort():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    logo = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, Y, O, O, O, O, O, O,
    L, Y, Y, P, P, Y, Y, B,
    L, G, Y, P, P, B, Y, Y,
    L, G, Y, P, B, Y, B, Y,
    L, L, L, L, L, L, L, L,
    O, L, O, O, O, O, L, O,
    ]
    return logo

def Mage1():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    logo = [
    G, G, G, G, Y, Y, O, B,
    G, Y, Y, Y, Y, O, O, Y,
    G, P, B, P, B, O, O, B,
    O, P, P, P, P, O, O, Y,
    B, B, Y, Y, B, B, O, B,
    P, Y, B, B, Y, P, O, Y,
    O, B, B, B, B, O, O, B,
    O, Y, O, O, Y, O, O, Y,
    ]
    return logo

def Mage2():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    logo = [
    G, G, G, G, Y, Y, O, B,
    G, Y, Y, Y, Y, O, O, Y,
    G, P, B, P, B, O, O, B,
    O, P, P, P, P, O, O, Y,
    O, B, P, Y, B, B, P, B,
    O, Y, B, B, Y, O, O, Y,
    O, B, B, B, B, O, O, B,
    O, Y, O, O, Y, O, O, Y,
    ]
    return logo

def Desert1():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    B, B, B, B, B, B, Y, Y, 
    B, B, B, B, B, B, Y, Y, 
    B, B, B, B, B, B, B, B, 
    B, B, B, B, B, B, B, B, 
    B, B, B, B, B, S, B, B, 
    B, B, B, B, S, S, S, B, 
    S, S, S, S, S, S, S, S,
    S, S, S, S, S, S, S, S,
        ]
    return logo

def Desert2():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    B, B, B, Y, Y, B, B, B, 
    B, B, B, Y, Y, B, B, B, 
    B, B, B, B, B, B, B, B, 
    B, B, B, B, B, B, B, B, 
    B, B, B, B, B, S, B, B, 
    B, B, B, B, S, S, S, B, 
    S, S, S, S, S, S, S, S,
    S, S, S, S, S, S, S, S,
        ]
    return logo
def Desert3():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    Y, Y, B, B, B, B, B, B, 
    Y, Y, B, B, B, B, B, B, 
    B, B, B, B, B, B, B, B, 
    B, B, B, B, B, B, B, B, 
    B, B, B, B, B, S, B, B, 
    B, B, B, B, S, S, S, B, 
    S, S, S, S, S, S, S, S,
    S, S, S, S, S, S, S, S,
        ]
    return logo

def ClosedDoor():
    Y = yellow
    O = nothing
    L= brun
    g = grey
    logo = [
    O, g, g, g, g, g, g, O,
    g, L, L, L, L, L, L, g,
    g, L, L, L, L, L, L, g,
    g, L, L, L, L, L, L, g,
    g, L, L, L, L, L, Y, g,
    g, L, L, L, L, L, L, g,
    g, L, L, L, L, L, L, g,
    g, L, L, L, L, L, L, g,
    ]
    return logo

def OpenDoor():
    Y = yellow
    O = nothing
    L= brun
    g = grey
    N = greyf
    logo = [
    g, g, g, N, N, N, N, O,
    g, L, g, O, O, O, O, N,
    g, L, g, O, O, O, O, N,
    g, L, g, O, O, O, O, N,
    g, Y, g, O, O, O, O, N,
    g, L, g, O, O, O, O, N,
    g, L, g, O, O, O, O, N,
    g, L, g, O, O, O, O, N,
    ]
    return logo

def Soleil():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    B, B, B, B, B, B, B, B,
    B, B, B, Y, Y, B, B, B,
    B, B, Y, Y, Y, Y, B, B,
    B, Y, Y, Y, Y, Y, Y, B,
    B, Y, Y, Y, Y, Y, Y, B,
    B, B, Y, Y, Y, Y, B, B,
    B, B, B, Y, Y, B, B, B,
    B, B, B, B, B, B, B, B, 
        ]
    return logo

def Sphynx():
    Y = yellow
    B = blue
    O = nothing
    L = brun
    S = sable
    logo = [
    O, B, Y, B, Y, B, O, O,
    O, S, S, S, S, S, Y, O,
    O, L, S, L, S, S, B, O,
    O, S, S, S, S, S, Y, O,
    O, S, L, S, S, B, S, S,
    O, Y, S, S, Y, S, S, S,
    O, B, S, S, B, S, S, S,
    S, S, S, S, S, S, S, S,
    ]
    return logo

def Lampe():
    Y = yellow
    O = nothing
    L = brun
    logo = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, Y, Y, Y, O, O, O,
    Y, Y, L, L, Y, Y, Y, Y,
    L, O, Y, Y, Y, Y, Y, O,
    L, L, L, Y, Y, Y, O, O,
    O, O, L, L, Y, O, O, O,
    O, L, L, Y, Y, Y, Y, O,
    ]
    return logo

def Genie1():
    Y = yellow
    B = blue
    O = nothing
    L = brun
    logo = [
    O, O, O, O, O, O, O, O,
    O, O, O, L, L, O, O, O,
    B, Y, O, B, B, O, Y, B,
    O, B, B, B, B, B, B, O,
    O, O, B, B, B, B, O, O,
    O, O, O, B, B, O, O, O,
    O, O, O, O, B, B, O, O,
    O, O, O, O, O, Y, Y, O,
    ]
    return logo

def Genie2():
    Y = yellow
    B = blue
    O = nothing
    L = brun
    logo = [
    O, O, O, L, L, O, O, O,
    B, Y, O, B, B, O, Y, B,
    O, B, B, B, B, B, B, O,
    O, O, B, B, B, B, O, O,
    O, O, O, B, B, O, O, O,
    O, O, O, O, B, B, O, O,
    O, O, O, O, O, B, O, O,
    O, O, O, O, O, Y, Y, O,
    ]
    return logo

def Argent():
    G = green
    Y = yellow
    B = blue
    O = nothing
    logo = [
    O, O, O, G, O, G, O, O,
    O, O, G, G, G, G, G, O,
    O, G, O, G, O, O, O, G,
    O, O, G, G, G, G, G, O,
    O, O, O, G, O, G, O, G,
    O, G, O, G, O, G, O, G,
    O, O, G, G, G, G, G, O,
    O, O, O, G, O, G, O, O,
    ]
    return logo

def Femme():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    M = brun
    logo = [
    O, M, Y, M, M, M, O, O,
    O, M, Y, P, P, P, O, O,
    M, M, P, B, P, B, M, O,
    M, M, P, P, P, P, M, O,
    P, B, B, Y, P, Y, B, P,
    O, O, B, B, Y, B, O, O,
    O, O, B, B, B, B, O, O,
    O, O, P, O, O, P, O, O,
    ]
    return logo

def Chameau1():
    Y = yellow
    B = blue
    L = brun
    logo = [
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,
    B, B, L, B, L, B, L, L,
    B, L, L, L, L, B, L, B,
    B, L, L, L, L, L, L, B,
    B, L, B, L, L, L, B, B,
    Y, L, Y, Y, Y, L, L, L,
    ]
    return logo

def Chameau2():
    Y = yellow
    B = blue
    L = brun
    logo = [
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, L, L,
    B, B, L, B, L, B, L, B,
    B, L, L, L, L, L, L, B,
    B, L, L, L, L, L, L, B,
    B, L, B, L, L, L, L, B,
    B, L, B, B, B, L, B, B,
    Y, L, Y, Y, Y, L, Y, Y,
    ]
    return logo

def DesertP1():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    B, B, B, B, B, B, Y, Y, 
    B, B, B, B, B, B, Y, Y, 
    B, B, B, B, B, B, B, B, 
    B, B, B, B, B, S, B, B, 
    B, B, B, B, L, L, S, B, 
    B, B, B, L, L, L, L, S, 
    S, S, L, L, L, L, L, L,
    S, L, L, L, L, L, L, S,
        ]
    return logo

def DesertP2():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    B, B, B, Y, Y, B, B, B, 
    B, B, B, Y, Y, B, B, B, 
    B, B, B, B, B, B, B, B,
    B, B, B, S, S, B, B, B, 
    B, B, S, L, L, S, B, B, 
    B, S, L, L, L, L, S, B, 
    S, L, L, L, L, L, L, S, 
    L, L, L, L, L, L, L, L,
        ]
    return logo

def DesertP3():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    Y, Y, B, B, B, B, B, B, 
    Y, Y, B, B, B, S, B, B, 
    B, B, B, B, S, S, L, B,
    B, B, B, S, S, L, L, L, 
    B, B, S, S, L, L, L, L, 
    B, S, S, L, L, L, L, L, 
    S, S, L, L, L, L, L, L, 
    S, L, L, L, L, L, L, L,
        ]
    return logo

def Pyramide():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    B, B, Y, Y, B, B, B, B, 
    B, Y, Y, Y, Y, B, B, B, 
    Y, Y, Y, Y, Y, Y, B, B, 
    Y, Y, Y, Y, Y, Y, Y, B, 
    S, S, S, S, S, Y, Y, Y, 
    L, S, L, S, L, S, Y, Y, 
    L, S, L, S, L, S, S, Y,   
    S, S, S, S, S, S, S, S, 
        ]
    return logo

def Porte1():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    S, S, S, S, S, S, S, S,
    S, Y, S, Y, S, Y, S, S,
    S, S, S, S, S, S, S, S,
    S, S, Y, S, Y, S, Y, S,
    S, S, S, S, S, S, S, S,
    S, Y, S, Y, S, Y, S, S,
    S, S, S, S, S, S, S, S,
    Y, Y, Y, Y, Y, Y, Y, Y,
        ]
    return logo

def Porte2():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    S, S, S, S, S, S, S, S,
    S, L, L, L, L, L, L, S,   
    S, Y, S, Y, S, Y, S, S,
    S, S, S, S, S, S, S, S,
    S, S, Y, S, Y, S, Y, S,
    S, S, S, S, S, S, S, S,
    S, Y, S, Y, S, Y, S, S,
    Y, Y, Y, Y, Y, Y, Y, Y,
        ]
    return logo

def Porte3():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    S, S, S, S, S, S, S, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, Y, S, Y, S, Y, S, S,
    S, S, S, S, S, S, S, S,
    S, S, Y, S, Y, S, Y, S,
    S, S, S, S, S, S, S, S,
    Y, Y, Y, Y, Y, Y, Y, Y,
        ]
    return logo

def Porte4():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    S, S, S, S, S, S, S, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, Y, S, Y, S, Y, S, S,
    S, S, S, S, S, S, S, S,
    S, S, Y, S, Y, S, Y, S,
    Y, Y, Y, Y, Y, Y, Y, Y,
        ]
    return logo

def Porte5():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    S, S, S, S, S, S, S, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, Y, S, Y, S, Y, S, S,
    S, S, S, S, S, S, S, S,
    Y, Y, Y, Y, Y, Y, Y, Y,
        ]
    return logo

def Porte6():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    S, S, S, S, S, S, S, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, Y, S, Y, S, Y, S, S,
    Y, Y, Y, Y, Y, Y, Y, Y,
        ]
    return logo

def Porte7():
    Y = yellow
    B = blue
    O = nothing
    P = pink
    G = grey
    L = brun
    S = sable
    R = red
    logo = [
    S, S, S, S, S, S, S, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    S, L, L, L, L, L, L, S,
    Y, Y, Y, Y, Y, Y, Y, Y,
        ]
    return logo

def Parchemin():
    Y = yellow
    B = blue
    G = grey
    L = brun
    logo = [
    B, B, L, Y, Y, B, B, B,
    B, L, Y, Y, G, Y, B, B,
    L, Y, G, Y, Y, Y, Y, B,
    Y, Y, G, G, Y, G, Y, Y,
    B, Y, G, Y, G, Y, G, Y,
    B, B, Y, Y, Y, G, Y, L,
    B, B, B, Y, G, Y, L, B,
    B, B, B, B, Y, L, B, B,
    ]
    return logo

def FlecheDroite():
    W = white
    O = nothing
    logo = [
    O, O, O, O, W, O, O, O,
    O, O, O, O, W, W, O, O,
    O, O, O, O, W, W, W, O,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    O, O, O, O, W, W, W, O,
    O, O, O, O, W, W, O, O,
    O, O, O, O, W, O, O, O,
    ]
    return logo

def FlecheGauche():
    W = white
    O = nothing
    logo = [
    O, O, O, W, O, O, O, O,
    O, O, W, W, O, O, O, O,
    O, W, W, W, O, O, O, O,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    O, W, W, W, O, O, O, O,
    O, O, W, W, O, O, O, O,
    O, O, O, W, O, O, O, O,
    ]
    return logo

def FlecheHaut():
    W = white
    O = nothing
    logo = [
    O, O, O, W, W, O, O, O,
    O, O, W, W, W, W, O, O,
    O, W, W, W, W, W, W, O,
    W, W, W, W, W, W, W, W,
    O, O, O, W, W, O, O, O,
    O, O, O, W, W, O, O, O,
    O, O, O, W, W, O, O, O,
    O, O, O, W, W, O, O, O,
    ]
    return logo

def FlecheBas():
    W = white
    O = nothing
    logo = [
    O, O, O, W, W, O, O, O,
    O, O, O, W, W, O, O, O,
    O, O, O, W, W, O, O, O,
    O, O, O, W, W, O, O, O,
    W, W, W, W, W, W, W, W,
    O, W, W, W, W, W, W, O,
    O, O, W, W, W, W, O, O,
    O, O, O, W, W, O, O, O,
    ]
    return logo

def BoutonPower():
    O = nothing
    R = red
    logo = [
    O, O, O, R, R, O, O, O,
    O, R, O, R, R, O, R, O,
    R, O, O, R, R, O, O, R,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O,
    ]
    return logo

def BoutonRestart():
    B = blue
    O = nothing
    logo = [
    O, O, O, B, O, O, O, O, 
    O, B, B, B, B, O, B, O, 
    B, O, O, B, O, O, O, B, 
    B, O, O, O, O, O, O, B, 
    B, O, O, O, O, O, O, B, 
    B, O, O, O, O, O, O, B, 
    O, B, O, O, O, O, B, O, 
    O, O, B, B, B, B, O, O, 
    ]
    return logo

def Question():
    O = nothing
    R = red
    logo = [
    O, O, O, R, R, R, O, O,
    O, O, R, O, O, O, R, O,
    O, O, O, O, O, O, R, O,
    O, O, O, O, O, R, O, O,
    O, O, O, R, R, O, O, O,
    O, O, O, R, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, R, O, O, O, O,
    ]
    return logo

def Trash():
    R = red
    O = nothing
    logo = [
    O, O, R, R, R, R, O, O,
    R, R, R, R, R, R, R, R,
    R, R, O, O, O, O, R, R,
    O, R, O, O, O, O, R, O,
    O, R, O, R, R, O, R, O,
    O, R, O, R, R, O, R, O,
    O, R, O, R, R, O, R, O,
    O, R, R, R, R, R, R, O,
    ]
    return logo

def Succes():
    G = green
    O = nothing
    logo = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, G,
    G, O, O, O, O, O, G, G,
    G, G, O, O, O, G, G, G,
    G, G, G, O, G, G, G, G,
    G, G, G, G, G, G, G, O,
    O, G, G, G, G, G, O, O,
    O, O, G, G, G, O, O, O,
    ]
    return logo

def Erreur():
    O = nothing
    R = red
    logo = [
    R, R, O, O, O, O, R, R,
    R, R, R, O, O, R, R, R,
    O, R, R, R, R, R, R, O,
    O, O, R, R, R, R, O, O,
    O, O, R, R, R, R, O, O,
    O, R, R, R, R, R, R, O,
    R, R, R, O, O, R, R, R,
    R, R, O, O, O, O, R, R,
    ]
    return logo
