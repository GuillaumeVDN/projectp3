# Project P3

This is a python project made for a raspberry pi with a sense hat ; the goal was to make a tool to encode and decode secret messages.

It's a school project for https://uclouvain.be/ made by Colla Quentin, Mathieu Taymans, Charlie Van Uffelen and Guillaume Vandenneucker.
