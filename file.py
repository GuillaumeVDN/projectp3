# coding=utf-8

import hashlib
import os

# nom du fichier
__file_name = "__message.txt"

# -------------------- Manipulation du fichier --------------------

def has_file():
    """Retourne True si un fichier est présent, sinon False"""
    try:
        f = open(__file_name, "r")
        f.close()
        return True
    except:
        return False

def delete_file():
    try:
        os.remove(__file_name)
    except:
        pass

# -------------------- Encryptage --------------------

def encrypt_file(message, password):
    try:
        with open(__file_name, "w") as f:
            hashcode = hashlib.md5(password.encode()).hexdigest()
            f.write(hashcode)
            f.write("\n")
            msg = code(message, password, 1)
            f.write(msg)
    except:
        pass

# -------------------- Décryptage --------------------

def decrypt_file(password):
    try:
        with open(__file_name, "r") as f:
            proposed_hashcode = hashlib.md5(password.encode()).hexdigest()
            if __is_hash_correct(proposed_hashcode):
                to_decrypt = f.readlines()[1].strip()
                decrypted = code(to_decrypt, password, -1)
                return decrypted
            else:
                return None
    except:
        pass

def __is_hash_correct(hashcode):
    """Return True si le hash est correct, sinon False"""
    f = open(__file_name, "r")
    correcthash  = f.readlines()[0].strip()
    if correcthash != hashcode:
        return False
    return True

# -------------------- Encodage/décodage --------------------

__letters = list('abcdefghijklmnopqrstuvwxyz0123456789')

def code(message, key, mode = 1):
    """pre: message et key des string, mode = 1 ou -1,
        post: retourne la chaîne encodée ou décodée si le mode est égal à 1 ou -1"""
    message = message.lower()
    result = ''
    i = 0
    for c in message:
        # caractère inconnu, on l'ajoute simplement
        if not c in __letters:
            result += c
        # caractère connu
        else:
            i += 1
            # trouver le caractère du mot de passe correspondant à cette position
            key_character = key[i % len(key)]
            # trouver un remplacement
            replacement_index = __letters.index(c) + mode * __letters.index(key_character)
            replacement = __letters[ replacement_index % len(__letters) ]
            # ajouter le remplacement au message
            result += replacement
    return result
